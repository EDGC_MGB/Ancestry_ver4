import os
import sys
import getopt
import re
import subprocess
import operator


APP_NAME='Ancestry-4.1.2'

def printParams():
	for parameter_val in Messege_print:
		print parameter_val

def printHelp():
	print '\n** parameters of '+APP_NAME+' are : '
	print '-f <Input file (Genotype file or VCF file) - Required >'
	print '--sr <Output path - Required>'
	print '--tp <Directory of temporary file - Required>'
	print '--uid <MGB user Id - Required>'
	print '--rn <Report Number - Required>' 
	print '--rl <Report language - Optional>\n'

def checkReportNum():
	if Report_num.isspace() or len(Report_num) == 0 or Report_num.startswith('--'):
		print '\nReport_num is empty! so cannot start '+APP_NAME
		printHelp()
		sys.exit()

def exit(msg):
	print 'aler^' + Report_num + '^' + Err_m[1] + ' ' + msg+'^alend;'
	sys.exit()

def checkParams():
	errs=[]

	def plus(m):
		errs.append(Report_num + '!' +m)

	if not os.path.isfile(Input_f):
		plus('invalid input file (-f : '+Input_f+')')

	if not os.path.isdir(Temp_dir):
		plus('invalid or inaccessible path (--tp : '+Temp_dir+')')

	if not os.path.isdir(Output_dir):
		plus('invalid or inaccessible path (--sr : '+Output_dir+')')

	if not User_id or not User_id.strip():
		plus('User_id is empty!')

	if len(errs) > 0:
		for err in errs:
			print err
		exit(str(len(errs)) + ' parameter error(s).')

def exitNotEnoughParam():
	exit('Not enough Parameter(s)')

def natural_sort(l):
	convert = lambda text: int(text) if text.isdigit() else text.lower()
	alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key)]
	return sorted(l, key = alphanum_key)

RUN_HOME=sys.path[0]

Err_m=[]
Error_messege_file=open(RUN_HOME+"/Error_messege_MGBApp.txt","r")
for line in Error_messege_file:
	Err_m.append(line.strip())

Report_num=''

##================================================================================================##
## Parameter
##================================================================================================##

try:
	if len(sys.argv[1:]) < 1:
		printHelp()
		sys.exit()

	paramList=sys.argv[1:]
	rn_idx=paramList.index('--rn')

	if rn_idx+1 > -1 and rn_idx <= len(paramList)-1:
		Report_num=paramList[rn_idx+1]
		checkReportNum()

	input_f=[]
	user_id=[]
	report_num=[]
	temp_dir=[]
	output_dir=[]

	Messege_print=[]

	def main(argv):
		try:
			opts, args = getopt.getopt(sys.argv[1:], "f:", ["uid=","rn=","rl=","sr=","tp=","rl="])

		except getopt.GetoptError:
			printHelp()
			exitNotEnoughParam()

		for opt, arg in opts:
			if opt in '-f':
				input_f.append(arg)
				Messege_print.append('f^'+arg)

			elif opt in '--uid':
				user_id.append(arg)
				Messege_print.append('uid^'+arg)

			elif opt in '--tp':
				arg=arg.rstrip('/')
				Messege_print.append('tp^'+arg)
				temp_dir.append(arg)

			elif opt in '--sr':
				arg=arg.rstrip('/')
				Messege_print.append('sr^'+arg)
				output_dir.append(arg)

			elif opt in '--rl':
				Messege_print.append('rl^'+arg)

	if __name__ == "__main__":
	   main(sys.argv[1:])

	Input_f=str("".join(input_f).strip())
	User_id=str("".join(user_id).strip())
	Temp_dir=str("".join(temp_dir).strip())
	Output_dir=str("".join(output_dir).strip())

	#CHECK Report_num first!
	checkReportNum()
	print 'alst^'+ Report_num + '^start^alend;'
	printParams()

	checkParams()

	##==============================================================================================================================================##
	##==============================================================================================================================================##
	## Step0. Converting Genotype to VCF format
	##==============================================================================================================================================##

	def mat_database(Sample_id, INPUT_RSID_DIC): # mat database
		
		mat_RSID1={}
		mat_1000g=open(Temp_dir+"/"+Sample_id+"_mat_af_1000genome_database.txt","w")
		with open(RUN_HOME+"/af_1000genome_database.txt","r") as dat_1000g:
			for line in dat_1000g:
				if not line.startswith("#"):
					ls=line.strip().split("\t")
					Chr, Loci, ref_rsid, Ref, Alt = "chr"+ls[0], ls[1], ls[2], ls[3], ls[4]
					if ref_rsid in INPUT_RSID_DIC:
						mat_RSID1[ref_rsid]=0
						mat_1000g.write("chr"+line)
		mat_1000g.close()

		mat_RSID2={}
		mat_afred_ref=open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_ref.txt","w")
		with open(RUN_HOME+"/Total_POP24_alfred_parsing_ref.txt", "r") as a_ref:
			for line in a_ref:
				if not line.startswith("population"):
					ls=line.strip().split("\t")
					RSID = ls[2]
					if RSID in INPUT_RSID_DIC:
						mat_RSID2[RSID]=0
						mat_afred_ref.write(line)
		mat_afred_ref.close()

		mat_afred_alt=open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_alt.txt","w")
		with open(RUN_HOME+"/Total_POP24_alfred_parsing_alt.txt", "r") as a_alt:
			for line2 in a_alt:
				if not line2.startswith("population"):
					ls=line2.strip().split("\t")
					RSID = ls[2]
					if RSID in INPUT_RSID_DIC:
						mat_afred_alt.write(line2)
		mat_afred_alt.close()

		return mat_RSID1, mat_RSID2


	def making_available_vcf(Sample_id):
		g1000_dic={}
		with open(Temp_dir+"/"+Sample_id+"_mat_af_1000genome_database.txt","r") as Mat_1000g:
			for line in Mat_1000g:
				if not line.startswith("#"):
					ls=line.strip().split("\t")
					Chr, Loci, ref_rsid, Ref, Alt = ls[0].split("chr")[1], ls[1], ls[2], ls[3], ls[4]
					db_key = Chr +"/"+ Loci +"/"+ ref_rsid +"/"+ Alt
					g1000_dic[db_key] = Chr+"\t"+Loci+"\t"+ref_rsid+"\t"+Ref+"\t"+Alt

		OUT_vcf=open(Temp_dir+"/"+Sample_id+".procesed.vcf","w")
		Alt_Homo={}
		for line in open(Input_f,"r"):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				if len(ls[3].strip()) == 2:
					RSID, CHR, POS, GT_allele1, GT_allele2 = ls[0], ls[1], ls[2], ls[3].strip()[0], ls[3].strip()[1]
					if CHR.startswith("chr"):
						CHR=CHR.split("chr")[1]
					if GT_allele1 != GT_allele2:
						Key1 = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele1
						Key2 = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele2
						if Key1 in g1000_dic : #0/1
							vcf_format="\t".join(g1000_dic[Key1].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"0/1:"+"\n"
							OUT_vcf.write(vcf_format)

						elif Key2 in g1000_dic :
							vcf_format="\t".join(g1000_dic[Key2].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"0/1:"+"\n"
							OUT_vcf.write(vcf_format)
						else:
							pass
					else:
						Key = CHR +"/"+ POS +"/"+RSID+"/"+ GT_allele1
						if Key in g1000_dic: #1/1
							vcf_format="\t".join(g1000_dic[Key].split("\t"))+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+"1/1:"+"\n"
							OUT_vcf.write(vcf_format)
		OUT_vcf.close()

	def re_mapping_dbsnp(dbsnp_database):
		dbmatching_dic={}
		with open(dbsnp_database, "r") as dbsnp_data:
			for line in dbsnp_data:
				if not line.startswith("#"):
					ls=line.strip().split("\t")
					Chr, Pos, RSID, Ref, Alt = ls[0], ls[1], ls[2], ls[3], ls[4]
					for alt in Alt.split(","):
						if Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in vcf_dic:
							VCF_RSID[RSID]=0
							if not Chr+"\t"+Pos+"\t"+Ref+"\t"+alt in dbmatching_dic:
								dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt]=[]
							dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt].append(RSID)

		out_mat_vcf=open(Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf", "w")
		
		with open(Input_f,"r") as f:
			for line in f:
				if line.startswith("#"):
					out_mat_vcf.write(line)
				else:
					ls=line.strip().split("\t")
					Chr, Pos, Ref, Alt = ls[0], ls[1], ls[3], ls[4]
					if Chr.startswith("chr"):
						Chr=ls[0].split("chr")[1]
					if Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt in dbmatching_dic:
						for val in dbmatching_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+Alt]:
							out_mat_vcf.write(Chr+"\t"+Pos+"\t"+val+"\t"+"\t".join(ls[3:])+"\n")
		out_mat_vcf.close()

		#global Input_f
		#Input_f=Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf"
		Input_ff=Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf"

		return Input_ff


	##===============================================================================================================================
	## if GT file
	##===============================================================================================================================
	if not ".vcf" in Input_f: #GT file
		
		INPUT_TYPE = "GT"
		
		Sample_id = Input_f.split("/")[-1].split(".txt")[0]

		GT_rsid={}
		for line in open(Input_f,"r"):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				if len(ls[3].strip()) == 2 and ls[0].startswith("rs"):
					RSID = ls[0]
					GT_rsid[RSID]=0

		##------------
		## function 
		mat_RSID1, mat_RSID2 = mat_database(Sample_id, GT_rsid)

		Ancestry_alt = open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_alt.txt", "r")
		Ancestry_ref = open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_ref.txt", "r")
		dat_1000g=Temp_dir+"/"+Sample_id+"_mat_af_1000genome_database.txt"

		#----------------------------------------------------------------------------------
		# common markers
		#----------------------------------------------------------------------------------

		#print "Common marker (1000genome): ", len(mat_RSID1)
		#print "Common marker (alfred): ", len(mat_RSID2)

		EAS_threshhold_val=10
		SAS_threshhold_val=5

		##length of using marker
		LENGTH_MARKER_1000=str(len(mat_RSID1))
		LENGTH_MARKER_alfred=str(len(mat_RSID2))

		if len(mat_RSID1) < 80000:
			print 'alca^' + Report_num + '^' +Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()
		if len(mat_RSID2) < 50000:
			print 'alca^' + Report_num + '^' +Err_m[0]+' QC_Fail' + '^alend;'
			sys.exit()

		##------------
		## function 
		making_available_vcf(Sample_id)
		
		temp_Vcf=str(Temp_dir+"/"+Sample_id+".procesed.vcf")
		
		os.system("sort -k1,1V -k2,2n "+ Temp_dir+"/"+Sample_id+".procesed.vcf" + " > "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf")
		os.system("cat "+RUN_HOME+"/vcf_header.txt "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf" +" > "+Temp_dir+"/"+Sample_id+".final.vcf")

		os.system("rm "+ Temp_dir+"/"+Sample_id+".procesed.vcf")
		os.system("rm "+ Temp_dir+"/"+Sample_id+".ordered.procesed.vcf")
		
	##===============================================================================================================================
	## if VCF file
	##===============================================================================================================================

	if ".vcf" in Input_f:
		#-----------------------------------------------------------
		VCF_RSID={}
		total_vcfLine=0
		with open(Input_f,"r") as f:
			for line in f:
				if not line.startswith("#"):
					total_vcfLine+=1
					ls=line.strip().split("\t")
					if len(ls) == 10 and ls[2].startswith("rs"):
						RSID = ls[2]
						VCF_RSID[RSID]=0

		##-----------------------------------------------------------
		## 01. QC check
		##-----------------------------------------------------------
		if float(len(VCF_RSID))/float(total_vcfLine) < 0.5:
			vcf_dic={}
			check_RefVersion=[]
			with open(Input_f,"r") as f:
				for line in f:
					if line.startswith("#"):
						check_RefVersion.append(line.strip())
					else:
						ls=line.strip().split("\t")
						Chr, Pos, Ref, Alt = ls[0], ls[1], ls[3], ls[4]
						if Chr.startswith("chr"):
							Chr=ls[0].split("chr")[1]
						for alt in Alt.split(","):
							vcf_dic[Chr+"\t"+Pos+"\t"+Ref+"\t"+alt]="\t".join(ls[5:])
			#----------------
			#version check
			#----------------
			VCF_version=""
			for val_line in check_RefVersion:
				if "hg38" in val_line.lower() or "grch38" in val_line.lower():
					VCF_version="hg38"
					break
				elif "hg19" in val_line.lower() or "grch37" in val_line.lower():
					VCF_version="hg19"
					break

			#----------------
			#version matching
			#----------------
			if VCF_version=="hg19":
				Input_f = re_mapping_dbsnp(RUN_HOME+"/RAW_661638_dbsnp_147.hg19.vcf")
				
			elif VCF_version=="hg38":
				Input_f = re_mapping_dbsnp(RUN_HOME+"/Ancestry740k_dbsnp_hg38.vcf")

			else:
				print 'alca^' + Report_num + '^' +Err_m[0]+' QC_Fail' + '^alend;'
				sys.exit()


		##-----------------------------------------------------------
		## 02. Check input type
		##-----------------------------------------------------------
		INPUT_TYPE=''

		command="wc -l "+ Input_f
		result_line = subprocess.Popen([command], stdout=subprocess.PIPE, shell=True)
		result_line = result_line.stdout.read().strip().split("\n")[-1].split(":")[-1].strip()
		result_line = result_line.strip().split("\n")[-1].split(":")[-1].strip()
		INPUT_LINES=int(result_line.split(" ")[0])

		#---------
		# WGS
		#---------
		if INPUT_LINES > 3000000:
			INPUT_TYPE="WGS"

			Ancestry_alt = open(RUN_HOME+"/Total_POP24_alfred_parsing_alt.txt", "r")
			Ancestry_ref = open(RUN_HOME+"/Total_POP24_alfred_parsing_ref.txt", "r")
			dat_1000g=RUN_HOME+"/af_GSA_std_cus_modicolumn.txt"

			LENGTH_MARKER_1000=str("GSA_Total")
			LENGTH_MARKER_alfred=str("Total")

			#EAS_threshhold_val=90
			#SAS_threshhold_val=50
			EAS_threshhold_val=0
			SAS_threshhold_val=0


		else:
			#---------
			# GT
			#---------
			Sample_id = Input_f.split("/")[-1].split(".vcf")[0]

			command2="grep '0/0' "+ Input_f+" |wc -l"
			result_f = subprocess.Popen([command2], stdout=subprocess.PIPE, shell=True)
			result_f = result_f.stdout.read()
			result_f = result_f.strip().split("\n")[-1].split(":")[-1].strip()
			INPUT_LINES_refhomo=int(result_f.split(" ")[0])

			EAS_threshhold_val=10
			SAS_threshhold_val=5


			if float(INPUT_LINES_refhomo)/float(INPUT_LINES) > 0.3:
				INPUT_TYPE="GT"
				#----------------------------------------------------------------------------------
				# mat database
				#----------------------------------------------------------------------------------
				#mat_RSID1={}
				#mat_RSID2={}
				mat_RSID1, mat_RSID2 = mat_database(Sample_id, VCF_RSID)
				
				#--------------------
				# common markers
				#--------------------
				#print "Common marker (1000genome): ", len(mat_RSID1)
				#print "Common marker (alfred): ", len(mat_RSID2)

				##length of using marker
				LENGTH_MARKER_1000=str(len(mat_RSID1))
				LENGTH_MARKER_alfred=str(len(mat_RSID2))

				if len(mat_RSID1) < 80000:
					os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0].split("_mat")[0]+"_mat.vcf")
					print 'alca^' + Report_num + '^' + Err_m[0]+' QC_Fail' + '^alend;'
					sys.exit()

				if len(mat_RSID2) < 50000:
					os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0].split("_mat")[0]+"_mat.vcf")
					print 'alca^' + Report_num + '^' + Err_m[0]+' QC_Fail' + '^alend;'
					sys.exit()


				Ancestry_alt = open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_alt.txt", "r")
				Ancestry_ref = open(Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_ref.txt", "r")
				dat_1000g=Temp_dir+"/"+Sample_id+"_mat_af_1000genome_database.txt"

			#---------
			# WES
			#---------
			else:
				INPUT_TYPE="WES"
				#print INPUT_TYPE
				Sample_id = Input_f.split("/")[-1].split(".vcf")[0]

				os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0].split("_mat")[0]+"_mat.vcf")
				print 'alca^' + Report_num + '^' + Err_m[0]+' QC_Fail' + '^alend;'
				sys.exit()

		#print INPUT_TYPE
			
		##----------------------------------
		## INPUT for eagle
		##----------------------------------

		Sample_id = Input_f.split("/")[-1].split(".vcf")[0]

		OUT_vcf=open(Temp_dir+"/"+Sample_id+".final.vcf","w")
		for line in open(Input_f,"r"):
			if line.startswith("#"):
				OUT_vcf.write(line)
			else:
				ls=line.strip().split("\t")
				if len(ls) == 10:
					if ls[9].strip().split(":")[0] in ["1/1","0/1"]:
						if ls[6]=="." and ls[7]==".":
							if ls[0].startswith("chr"): ## Convert character "chr1" to "1"
								OUT_vcf.write(ls[0].split("chr")[1]+"\t"+"\t".join(ls[1:5])+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+ls[9].strip().split(":")[0]+":"+"\n")
							else:
								OUT_vcf.write("\t".join(ls[:5])+"\t20000\tPASS\tDP=630;FS=0.000;MQ=60.00;QD=46.96\tGT:AD:DP:GQ:PL\t"+ls[9].strip().split(":")[0]+":"+"\n")
						else:
							if ls[0].startswith("chr"):
								OUT_vcf.write(ls[0].split("chr")[1]+"\t"+"\t".join(ls[1:])+"\n") ## Convert character "chr1" to "1"
							else:
								OUT_vcf.write("\t".join(ls[0:])+"\n") 
		OUT_vcf.close()


	##==================================================================================================================================================
	##--------------------------------------------------------------------------------------------------------------------------------------------------
	## Run Eagle (for phasing)
	##--------------------------------------------------------------------------------------------------------------------------------------------------
	##==================================================================================================================================================
	## setting Eagles

	eagle_out=Sample_id+'.phased'

	os.system("bgzip -f "+Temp_dir+"/"+Sample_id+".final.vcf")
	os.system("tabix -p vcf "+Temp_dir+"/"+Sample_id+".final.vcf.gz")
					
	## run eagle

	os.system(RUN_HOME+"/eagle --vcfRef="+RUN_HOME+"/ref.50samples.phase3.20130502.genotypes.vcf.gz --vcfTarget="+Temp_dir+"/"+Sample_id+".final.vcf.gz"+" --geneticMapFile="+RUN_HOME+"/genetic_map_hg19.txt.gz --outPrefix="+Temp_dir+"/"+eagle_out+" 2>&1 | tee "+ Temp_dir+"/"+Sample_id+".eagle.log > /dev/null")

	os.system("gzip -fd "+Temp_dir+"/"+eagle_out+".vcf.gz")

	os.system("rm -f "+ Temp_dir+"/"+Sample_id+".final.vcf.gz")
	os.system("rm -f "+ Temp_dir+"/"+Sample_id+".final.vcf.gz.tbi")


	## clear
	VCF_RSID={}
	GT_rsid={}
	mat_RSID1={}
	mat_RSID2={}
	g1000_dic={}

	##==================================================================================================##
	## Step2. Calculate Ancestry algorithm for chromosome map 
	##
	## INPUT : phased vcf file after Beagle
	##
	## MID_OUTPUT : _Ancestry_hp1.txt / _Ancestry_hp2.txt
	##
	## OUTPUT : 
	##==================================================================================================##

	try:
		phasing_vcf=open(Temp_dir+"/"+eagle_out+".vcf", 'r')
	except:
		print 'aler^' + Report_num + '^' + Err_m[1]+" No eagle output" + '^alend;'
		sys.exit()


	##=====================================================================================================##
	## DATABASE
	##
	## Reference dbSNP data (parsing data)
	##
	## Need requisitely dbsnp that is relevant to ancestry total snp
	## Must be check number of dbsnp and ancestry snp
	##=====================================================================================================##

	##=====================================================================================================##
	## Alfred database (parsing data-used major populations and snp)
	##=====================================================================================================##

	##---------------------------------------------------------
	## alt_dic={RSID_Alt:{Pop:Freq}}
	##---------------------------------------------------------

	alt_dic={}
	Ancestry_rsid={}
	Ancestry_pop={}
	for line in Ancestry_alt:
		if not line.startswith("population"):
			ls=line.strip().split("\t")
			Pop, Size, RSID, alt, Freq = ls[0], ls[1], ls[2], ls[3], float(ls[4].strip())
			Ancestry_rsid[RSID]=0
			Ancestry_pop[Pop]=0
			KEY=RSID+":"+alt
			if KEY not in alt_dic:
				alt_dic[KEY]={Pop:Freq}
			if Pop not in alt_dic[KEY]:
				alt_dic[KEY][Pop]=Freq
			alt_dic[KEY][Pop]=Freq
	Ancestry_alt.close()

	## Dictionary of reference allele of Ancestry ----------------------------------------------------------##
	## ref_dic={RSID:{Pop:Freq}}
	##----------------
	## Ref allele
	##----------------
	ref_dic={}

	for line2 in Ancestry_ref:
		if not line2.startswith("population"):
			ls=line2.strip().split("\t")
			Pop, Size, RSID, Ref, Freq = ls[0], ls[1], ls[2], ls[3], float(ls[4].strip())
			Ancestry_rsid[RSID]=0
			KEY=RSID+":"+Ref
			if KEY not in ref_dic:
				ref_dic[KEY]={Pop:Freq}
			else:
				ref_dic[KEY][Pop]=Freq
	Ancestry_ref.close()

	ref_population = sorted(Ancestry_pop.keys())  ### populations list of Ancestry(24)

	## Reference of Chromosome length ----------------------------------------------------------------------##

	reference_chr=open(RUN_HOME+"/Reference_LengthOfChromosome.txt","r")
	Chr_BINDIC={}
	win_reading_size = 1000000 #1M
	win_bin_size = 5000000 #5M
	for line in reference_chr:
		ls=line.strip().split("\t")
		Chr, Length =  ls[0], int(ls[2])
		Chr_BINDIC[Chr] = {}

		for i in range(0, Length, win_reading_size):
			start_point = i
			Chr_BINDIC[Chr][start_point] = 0

	##=====================================================================================================##
	## 1000genome af database 
	##=====================================================================================================##
	#=============>dat_1000g=open(Temp_dir+"/mat_af_1000genome_database.txt","r")

	g1000_marker={}
	af_alt_dic={}
	af_ref_dic={}
	Dat_1000g=open(dat_1000g, "r")
	for line in Dat_1000g:
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Chr, Pos, Rsid, Ref, Alt = ls[0], ls[1], ls[2], ls[3], ls[4]
			
			if Rsid != ".":
				EAS_f, SAS_f, EUR_f, AMR_f, AFR_f = float(ls[5]), float(ls[6]), float(ls[7]), float(ls[8]), float(ls[9])
				g1000_marker[Rsid]=0
				##=====================================
				## alt af
				##=====================================		
				key_alt=Rsid+":"+Alt

				if key_alt not in af_alt_dic:
					af_alt_dic[key_alt]={"EAS":EAS_f}			
				af_alt_dic[key_alt]["EAS"]=EAS_f

				if key_alt not in af_alt_dic:
					af_alt_dic[key_alt]={"SAS":SAS_f}			
				af_alt_dic[key_alt]["SAS"]=SAS_f

				if key_alt not in af_alt_dic:
					af_alt_dic[key_alt]={"EUR":EUR_f}			
				af_alt_dic[key_alt]["EUR"]=EUR_f

				if key_alt not in af_alt_dic:
					af_alt_dic[key_alt]={"AMR":AMR_f}			
				af_alt_dic[key_alt]["AMR"]=AMR_f

				if key_alt not in af_alt_dic:
					af_alt_dic[key_alt]={"AFR":AFR_f}			
				af_alt_dic[key_alt]["AFR"]=AFR_f

				##=====================================
				## ref af
				##=====================================
				key_ref=Rsid+":"+Ref

				if key_ref not in af_ref_dic:
					af_ref_dic[key_ref]={"EAS":1-EAS_f}			
				af_ref_dic[key_ref]["EAS"]=1-EAS_f

				if key_ref not in af_ref_dic:
					af_ref_dic[key_ref]={"SAS":1-SAS_f}			
				af_ref_dic[key_ref]["SAS"]=1-SAS_f

				if key_ref not in af_ref_dic:
					af_ref_dic[key_ref]={"EUR":1-EUR_f}			
				af_ref_dic[key_ref]["EUR"]=1-EUR_f

				if key_ref not in af_ref_dic:
					af_ref_dic[key_ref]={"AMR":1-AMR_f}			
				af_ref_dic[key_ref]["AMR"]=1-AMR_f

				if key_ref not in af_ref_dic:
					af_ref_dic[key_ref]={"AFR":1-AFR_f}			
				af_ref_dic[key_ref]["AFR"]=1-AFR_f

	Dat_1000g.close()
	pop_1000=["EAS", "SAS", "EUR", "AMR", "AFR"]


	#print len(af_ref_dic)
	#print len(af_alt_dic)
	#print len(g1000_marker)
	##=======================================================================================================##
	##
	## Algorism
	##
	##=======================================================================================================##

	##---------------------------
	## -- VCF file reading  --
	## Making vcf dictionary !!
	##---------------------------

	#print "start vcf calculating"
	vcf_a1_alt_dic={} #vcf_a1_alt_dic[Chr]={Loci:line_f}
	vcf_a1_ref_dic={}
	vcf_a2_alt_dic={}
	vcf_a2_ref_dic={}

	vcf_marker={}
	for line in phasing_vcf:
		if not line.startswith("#"):
			ls=line.split()
			Chr, Loci, vcf_rsid, Ref, Alt, Phasing_infor = "chr"+ls[0], int(ls[1]), ls[2], ls[3], ls[4], ls[9]
			#line_f=Chr+"\t"+Loci+"\t"+vcf_rsid+"\t"+Ref+"\t"+Alt+"\t"+Phasing_infor
			line_f=vcf_rsid+"\t"+Ref+"\t"+Alt

			vcf_marker[vcf_rsid]=0
			if not vcf_rsid == ".":
				Phasing_allele1=Phasing_infor.split("|")[0]
				Phasing_allele2=Phasing_infor.split("|")[1].split(":")[0]
				
				## Allele 1 : ---------------------------------------------##
				if Phasing_allele1 == "1": 
					if Chr not in vcf_a1_alt_dic:
						vcf_a1_alt_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a1_alt_dic[Chr]:
						vcf_a1_alt_dic[Chr][Loci]=line_f
					vcf_a1_alt_dic[Chr][Loci]=line_f
				
				if Phasing_allele1 =="0":
					if Chr not in vcf_a1_ref_dic:
						vcf_a1_ref_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a1_ref_dic[Chr]:
						vcf_a1_ref_dic[Chr][Loci]=line_f
					vcf_a1_ref_dic[Chr][Loci]=line_f
				
				## Allele 2 : ---------------------------------------------##
				if Phasing_allele2 == "1": 
					if Chr not in vcf_a2_alt_dic:
						vcf_a2_alt_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a2_alt_dic[Chr]:
						vcf_a2_alt_dic[Chr][Loci]=line_f
					vcf_a2_alt_dic[Chr][Loci]=line_f
					
				if Phasing_allele2 =="0":
					if Chr not in vcf_a2_ref_dic:
						vcf_a2_ref_dic[Chr]={Loci:line_f}
					if Loci not in vcf_a2_ref_dic[Chr]:
						vcf_a2_ref_dic[Chr][Loci]=line_f
					vcf_a2_ref_dic[Chr][Loci]=line_f


	##---------------------------------------------------------------------------------------------------##
	## No marker in vcf --> ref allele 
	##---------------------------------------------------------------------------------------------------##
	ref_key={}
	for key in Ancestry_rsid:
		if key in vcf_marker:
			pass
		else:
			ref_key[key]=0

	ref2_key={}
	for key in g1000_marker:
		if key in vcf_marker:
			pass
		else:
			ref2_key[key]=0

	##---------------------------------------------------------------------------------------------------##
	## Need ref vcf file (dbSNP)
	##---------------------------------------------------------------------------------------------------##

	dbSNP_dic={}
	with open(RUN_HOME+"/RAW_661638_dbsnp_147.hg19.vcf", "r") as ref_dbsnp:
		for ref_vcf in ref_dbsnp:
			ls=ref_vcf.split("\t")
			Chr, Loci, ref_rsid, Ref = "chr"+ls[0], int(ls[1]), ls[2], ls[3]
			line_f=ref_rsid+"\t"+Ref+"\t"+Ref

			if ref_rsid in ref_key:
				if Chr not in dbSNP_dic:
					dbSNP_dic[Chr]={Loci:line_f}
				if Loci not in dbSNP_dic[Chr]:
					dbSNP_dic[Chr][Loci]=line_f
				dbSNP_dic[Chr][Loci]=line_f


	g1000_ref_dic={}
	#with open(Temp_dir+"/mat_af_1000genome_database.txt","r") as dat_1000g:
	Dat_1000g=open(dat_1000g, "r")
	for line in Dat_1000g:
		if not line.startswith("#"):
			ls=line.strip().split("\t")
			Chr, Loci, ref_rsid, Ref = ls[0], int(ls[1]), ls[2], ls[3]
			line_f=ref_rsid+"\t"+Ref+"\t"+Ref
			if ref_rsid in ref2_key:
				if Chr not in g1000_ref_dic:
					g1000_ref_dic[Chr]={Loci:line_f}
				if Loci not in g1000_ref_dic[Chr]:
					g1000_ref_dic[Chr][Loci]=line_f
				g1000_ref_dic[Chr][Loci]=line_f

	Dat_1000g.close()


	##------------------------------------------------------------------------------------------------##
	## Calculate window reading
	##------------------------------------------------------------------------------------------------##

	def Algorism_1_calculation_population(Chr_infor, Chr_BINDIC, alt_dic, ref_dic, vcf_a1_alt_dic, vcf_a1_ref_dic, POP_dic, count_dic, Bin_dic):
		sub_dic={}
		if Chr_infor in vcf_a1_alt_dic:
			for j in vcf_a1_alt_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):
						vcf_rsid = vcf_a1_alt_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_alt_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_alt_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Alt
						if key in alt_dic:
							sub_dic = alt_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_dic.keys():
								if bin_val not in POP_dic[Chr_infor][ethnic]:
									POP_dic[Chr_infor][ethnic][bin_val] = 0
								POP_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count_dic[Chr_infor][ethnic]:
									count_dic[Chr_infor][ethnic][bin_val] = 0
								count_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0

		if Chr_infor in vcf_a1_ref_dic:
			for j in vcf_a1_ref_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = vcf_a1_ref_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= vcf_a1_ref_dic[Chr_infor][j].split("\t")[1]
						vcf_Alt = vcf_a1_ref_dic[Chr_infor][j].split("\t")[2]

						key=vcf_rsid+":"+vcf_Ref
						if key in ref_dic:
							sub_dic = ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}
							for ethnic in sub_dic.keys():
								if bin_val not in POP_dic[Chr_infor][ethnic]:
									POP_dic[Chr_infor][ethnic][bin_val] = 0
								POP_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count_dic[Chr_infor][ethnic]:
									count_dic[Chr_infor][ethnic][bin_val] = 0
								count_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0
		
		return POP_dic, count_dic

	def Algorism_2_calculation_refhomo(Chr_infor, Chr_BINDIC, database_dic, ref_dic, POP1_dic, POP2_dic, count1_dic, count2_dic, Bin_dic):
		
		sub_dic={}
		if Chr_infor in database_dic:
			for j in database_dic[Chr_infor]:
				for bin_val in Chr_BINDIC[Chr_infor]:
					if (j-win_bin_size <= bin_val) and (bin_val <= j):

						vcf_rsid = database_dic[Chr_infor][j].split("\t")[0]
						vcf_Ref= database_dic[Chr_infor][j].split("\t")[1]

						key=vcf_rsid+":"+vcf_Ref
						if key in ref_dic:
							sub_dic = ref_dic[key] #alt_dic[rsid:allele]={Pop:Freq}

							for ethnic in sub_dic.keys():
								if bin_val not in POP1_dic[Chr_infor][ethnic]:
									POP1_dic[Chr_infor][ethnic][bin_val] = 0
								POP1_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count1_dic[Chr_infor][ethnic]:
									count1_dic[Chr_infor][ethnic][bin_val] = 0
								count1_dic[Chr_infor][ethnic][bin_val] += 1

							for ethnic in sub_dic.keys():
								if bin_val not in POP2_dic[Chr_infor][ethnic]:
									POP2_dic[Chr_infor][ethnic][bin_val] = 0
								POP2_dic[Chr_infor][ethnic][bin_val] += sub_dic[ethnic]

								if bin_val not in count2_dic[Chr_infor][ethnic]:
									count2_dic[Chr_infor][ethnic][bin_val] = 0
								count2_dic[Chr_infor][ethnic][bin_val] += 1

						Bin_dic[Chr_infor][bin_val] = 0
		
		return POP1_dic, POP2_dic, count1_dic, count2_dic


	CHR_LIST=[]
	for i in range(1,23):
		CHR_LIST.append("chr"+str(i))

	Chromosome={}

	POP1_dic={} ### Allele1_Final result of algorism (Sumation of frequency per chr bin)
	POP2_dic={} ### Allele2_Final result of algorism (Sumation of frequency per chr bin)
	count1_dic={}
	count2_dic={}

	POP1_1000={} 
	POP2_1000={} 
	count1_1000={}
	count2_1000={}

	Bin_dic={}

	for Chr_infor in natural_sort(CHR_LIST):
		#print "===", Chr_infor, "==="
		#if Chr_infor == "chr22":
		Chromosome[Chr_infor]=0
		POP1_dic[Chr_infor] = {}
		POP2_dic[Chr_infor] = {}
		Bin_dic[Chr_infor] = {}
		count1_dic[Chr_infor] = {}
		count2_dic[Chr_infor] = {}

		for ethnicity in ref_population:
			POP1_dic[Chr_infor][ethnicity] = {}
			POP2_dic[Chr_infor][ethnicity] = {}
			count1_dic[Chr_infor][ethnicity] = {}
			count2_dic[Chr_infor][ethnicity] = {}

		##----------------------------------------------------------------------------
		## Calculating Afred database !!!
		##----------------------------------------------------------------------------
		## Allele 1
		POP1_dic, count1_dic = Algorism_1_calculation_population(Chr_infor, Chr_BINDIC, alt_dic, ref_dic, vcf_a1_alt_dic, vcf_a1_ref_dic, POP1_dic, count1_dic, Bin_dic)
		## Allele 2 ------------------------------------------------------------------
		POP2_dic, count2_dic = Algorism_1_calculation_population(Chr_infor, Chr_BINDIC, alt_dic, ref_dic, vcf_a2_alt_dic, vcf_a2_ref_dic, POP2_dic, count2_dic, Bin_dic)
		## No vcf (Ref) ------------------------------------------------------------------
		POP1_refhomo_dic, POP2_refhomo_dic, count1_refhomo_dic, count2_refhomo_dic = Algorism_2_calculation_refhomo(Chr_infor, Chr_BINDIC, dbSNP_dic, ref_dic, POP1_dic, POP2_dic, count1_dic, count2_dic, Bin_dic)
		
		POP1_dic.update(POP1_refhomo_dic)
		POP2_dic.update(POP2_refhomo_dic)
		count1_dic.update(count1_refhomo_dic)
		count2_dic.update(count2_refhomo_dic)


		##----------------------------------------------------------------------------
		## Calculating 1000 genome database !!!
		##----------------------------------------------------------------------------
		POP1_1000[Chr_infor] = {}
		POP2_1000[Chr_infor] = {}
		count1_1000[Chr_infor] = {}
		count2_1000[Chr_infor] = {}
		#Bin_dic[Chr_infor] = {}
		for ethnicity in pop_1000:
			POP1_1000[Chr_infor][ethnicity] = {}
			POP2_1000[Chr_infor][ethnicity] = {}
			count1_1000[Chr_infor][ethnicity] = {}
			count2_1000[Chr_infor][ethnicity] = {}

		## Allele 1 ------------------------------------------------------------------
		POP1_1000, count1_1000 = Algorism_1_calculation_population(Chr_infor, Chr_BINDIC, af_alt_dic, af_ref_dic, vcf_a1_alt_dic, vcf_a1_ref_dic, POP1_1000, count1_1000, Bin_dic)
		## Allele 2 ------------------------------------------------------------------
		POP2_1000, count2_1000 = Algorism_1_calculation_population(Chr_infor, Chr_BINDIC, af_alt_dic, af_ref_dic, vcf_a2_alt_dic, vcf_a2_ref_dic, POP2_1000, count2_1000, Bin_dic)
		## No vcf (Ref) ------------------------------------------------------------------
		POP1_1000_refhomo_dic, POP2_1000_refhomo_dic, count1_1000_refhomo_dic, count2_1000_refhomo_dic = Algorism_2_calculation_refhomo(Chr_infor, Chr_BINDIC, g1000_ref_dic, af_ref_dic, POP1_1000, POP2_1000, count1_1000, count2_1000, Bin_dic)

		POP1_1000.update(POP1_1000_refhomo_dic)
		POP2_1000.update(POP2_1000_refhomo_dic)
		count1_1000.update(count1_1000_refhomo_dic)
		count2_1000.update(count2_1000_refhomo_dic)


	#print "done... calculate (window reading)"
	##===============================================================================================================##
	##
	## Calculate final pop
	##
	##===============================================================================================================##

	##---------------------------------------------------------------
	## Reference
	##---------------------------------------------------------------

	Desition_dic={} #{1:pop, 2: pop, 3: pop ...}
	for index in range(len(ref_population)):
		Desition_dic[index]=ref_population[index]

	#print Desition_dic.keys()

	Desition_1000={}
	for index in range(len(pop_1000)):
		Desition_1000[index]=pop_1000[index]

	##----------------------------------------------------------------
	## start calculate
	##----------------------------------------------------------------

	def Algorism_3_calculation_define_population(Chromosome, POP1_dic, POP1_1000, count1_dic, count1_1000, mid_out1, mid_out1_1000, mid_out1_modi, mid_out1_modi_final):
		Mid_out1=open(mid_out1, "w")
		Mid_out1.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred'+"\n")

		Mid_out1_1000=open(mid_out1_1000, "w")
		Mid_out1_1000.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(pop_1000)+'\tDesition1000\tDesition_alfred'+"\n")

		Mid_out1_modi=open(mid_out1_modi, "w")
		Mid_out1_modi.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred\tModi1000'+"\n")

		Mid_out1_modi_final=open(mid_out1_modi_final,"w")
		Mid_out1_modi_final.write('#Chr\tBin\tCount_1000\tCount_alfred\t'+"\t".join(ref_population)+'\tDesition1000\tDesition_alfred\tModi1000\tModiPOP'+"\n")

		##-------------------------------------
		## Firstly, checking 1000genome result
		##-------------------------------------
		modi_pop1=[]
		for Chr in natural_sort(Chromosome.keys()):
			if Chr in Bin_dic:
				m_bin = sorted(Bin_dic[Chr])
				for posBin in m_bin:
					m_binFreq = []
					Count1=[]

					binFreq1000=[]
					final_1000POP=[]
					Count1_1000=[]
					
					## alfred pop
					for popName in ref_population:
						try:m_binFreq.append(float('%0.4f' %(POP1_dic[Chr][popName][posBin])))
						except:m_binFreq.append(float('0'))
						try:Count1.append(int(count1_dic[Chr][popName][posBin]))
						except:Count1.append('0')

					## 1000 pop
					for pop_name in pop_1000:
						try:binFreq1000.append(float('%0.4f' %(POP1_1000[Chr][pop_name][posBin]))) 
						except:binFreq1000.append(float('0'))
						try:Count1_1000.append(int(count1_1000[Chr][pop_name][posBin]))
						except:Count1_1000.append('0')

					if sum(m_binFreq) != 0 and len(m_binFreq) == 24 :
						## Final population
						final_count1 = max(Count1) 
						final_count = max(Count1_1000)

						if final_count > 10 and final_count1 > 10:
							final_index1000 = binFreq1000.index(max(binFreq1000)) ## 1000 pop
							final_1000POP.append(Desition_1000[final_index1000])
						else:
							final_1000POP.append("NA")
						modi_pop1.append(''.join(final_1000POP))	

		MAIN_1000_result={}
		MAIN_RESULT=''
		for val_list in pop_1000:
			MAIN_1000_result[val_list]=modi_pop1.count(val_list)
			MAIN_RESULT=max(MAIN_1000_result.iteritems(), key=operator.itemgetter(1))[0]

		#print MAIN_1000_result
		#print MAIN_RESULT

		##---------------------------------------------------------------------
		## Calculate popultations (allele1)
		##---------------------------------------------------------------------

		modii_pop1=[]
		for Chr in natural_sort(Chromosome.keys()):
			if Chr in Bin_dic:
				m_bin = sorted(Bin_dic[Chr])
				for posBin in m_bin:
					m_binFreq = []
					m_binFreq2=[]
					#final_POP = []
					Count1=[]

					binFreq1000=[]
					binFreq1000_2=[]
					final_1000POP=[]
					Count1_1000=[]
					
					## alfred pop
					for popName in ref_population:
						#print popName
						try:m_binFreq.append(float('%0.4f' %(POP1_dic[Chr][popName][posBin])))
						except:m_binFreq.append(float('0'))
						try:m_binFreq2.append('%0.4f' %(POP1_dic[Chr][popName][posBin]))
						except:m_binFreq2.append('0')
						try:Count1.append(int(count1_dic[Chr][popName][posBin]))
						except:Count1.append('0')

					## 1000 pop
					for pop_name in pop_1000:
						try:binFreq1000.append(float('%0.4f' %(POP1_1000[Chr][pop_name][posBin]))) 
						except:binFreq1000.append(float('0'))
						try:binFreq1000_2.append('%0.4f' %(POP1_1000[Chr][pop_name][posBin]))
						except:binFreq1000_2.append('0')
						try:Count1_1000.append(int(count1_1000[Chr][pop_name][posBin]))
						except:Count1_1000.append('0')


					if sum(m_binFreq) != 0 and len(m_binFreq) == 24 :

						final_count1 = max(Count1) 
						final_count = max(Count1_1000)

						final_POP = []

						## Final population
						if final_count > 10 and final_count1 > 10:

							final_index1000 = binFreq1000.index(max(binFreq1000)) ## 1000 pop
							final_1000POP.append(Desition_1000[final_index1000])

							if final_1000POP == ['AFR']: 
								final_index1 = m_binFreq[0:3].index(max(m_binFreq[0:3])) ## alfred pop
								final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

							elif final_1000POP == ['EUR']: 
								final_index4 = (m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:]).index(max(m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:])) ## Oceania, Siberia
								final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])

							elif final_1000POP == ['SAS']: #South Asian
								final_index2 = m_binFreq[3:6].index(max(m_binFreq[3:6])) ## alfred pop
								final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

								##------------------
								## 1000genome modi
								##------------------
								if MAIN_RESULT=='EUR':
									SAS_1000=float(binFreq1000[1])
									EUR_1000=float(binFreq1000[2])
									if abs(SAS_1000-EUR_1000) < SAS_threshhold_val:
										final_1000POP=[]
										final_1000POP.append("EUR")

										final_POP=[]
										final_index4 = (m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:]).index(max(m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:])) ## Oceania, Siberia
										final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])

							elif final_1000POP == ['EAS']: 
								final_index3 = m_binFreq[6:9].index(max(m_binFreq[6:9])) ## alfred pop
							
								Han=float(m_binFreq[6])
								Japan=float(m_binFreq[7])
								Korean=float(m_binFreq[8])

								if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
									final_POP.append("EastAsia:Koreans")
								elif ''.join(final_POP) == "EastAsia:Han":
									final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
								else:
									final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

								##------------------
								## 1000genome modi
								##------------------
								if MAIN_RESULT=='EUR':
									EAS_1000=float(binFreq1000[0])
									EUR_1000=float(binFreq1000[2])
									if abs(EAS_1000-EUR_1000) < EAS_threshhold_val:
										final_1000POP=[]
										final_1000POP.append("EUR")

										final_POP=[]
										final_index4 = (m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:]).index(max(m_binFreq[9:11]+m_binFreq[13:17]+m_binFreq[20:])) ## Oceania, Siberia
										final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])

							elif final_1000POP == ['AMR']: 
								final_index5 = (m_binFreq[11:13]+m_binFreq[17:20]).index(max(m_binFreq[11:13]+m_binFreq[17:20])) ## alfred pop
								final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])

						else:
							final_POP.append("NA")
							final_1000POP.append("NA")
					
						modii_pop1.append(''.join(final_1000POP))	
						Mid_out1.write(Chr+"\t"+str(posBin)+"\t"+str(final_count)+"\t"+str(final_count1)+"\t"+"\t".join(m_binFreq2)+"\t"+''.join(final_1000POP)+'\t'+''.join(final_POP)+'\n')
						Mid_out1_1000.write(Chr+'\t'+str(posBin)+'\t'+str(final_count)+"\t"+str(final_count1)+"\t"+"\t".join(binFreq1000_2)+"\t"+''.join(final_1000POP)+'\t'+''.join(final_POP)+'\n')

		Mid_out1.close()
		Mid_out1_1000.close()

		#--------------------------------------------------------------------------------------------
		# modi popultations - Allele1 (Updat. 17.06.15)
		#--------------------------------------------------------------------------------------------
		pop_lenth=len(modii_pop1)
		Final_modiPOP1=[]
		for i, k in zip(range(0,pop_lenth+1, 10), range(0,pop_lenth+1, 10)[1:]+[len(modii_pop1)]):
			#print i,k, modii_pop1[i:k]

			if "NA" in modii_pop1[i:k]:
				Final_modiPOP1.extend(modii_pop1[i:k])

			else:
				if len(set(modii_pop1[i:k]))==1:
					Final_modiPOP1.extend(modii_pop1[i:k])
				elif 1 <= len(set(modii_pop1[i:k])) <= 2:
					modi_key=dict((x, modii_pop1[i:k].count(x)) for x in sorted(list(set(modii_pop1[i:k])))).keys()
					modi_list=dict((x, modii_pop1[i:k].count(x)) for x in sorted(list(set(modii_pop1[i:k])))).values()
					major_index=modi_list.index(max(modi_list))
					major_pop=modi_key[major_index]

					Final_modiPOP1.extend([major_pop]*10)

				else:
					Final_modiPOP1.extend(modii_pop1[i:k])

		# re-output-------------------------------------------------------------------------------------------

		modi_count1=0
		for i, line in zip(range(0,len(Final_modiPOP1)+1), open(mid_out1,"r")):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				Mid_out1_modi.write("\t".join(ls)+"\t"+Final_modiPOP1[i-1]+"\n")

		Mid_out1_modi.close()

		# moid pop2-------------------------------------------------------------------------------------------

		result_pop=[]
		for line in open(mid_out1_modi,"r"):
			if not line.startswith("#"):
				ls=line.strip().split("\t")
				Final_modiPOP = ls[-1]
				pop_ratio=ls[4:28]

				final_POP=[]

				if Final_modiPOP == 'AFR': 
					final_index1 = pop_ratio[0:3].index(max(pop_ratio[0:3])) ## alfred pop
					final_POP.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])
					result_pop.append(Desition_dic[Desition_dic.keys()[0:3][final_index1]])

				elif Final_modiPOP == 'SAS': #South Asian
					final_index2 = pop_ratio[3:6].index(max(pop_ratio[3:6])) ## alfred pop
					final_POP.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])
					result_pop.append(Desition_dic[Desition_dic.keys()[3:6][final_index2]])

				elif Final_modiPOP == 'EAS': 
					final_index3 = pop_ratio[6:9].index(max(pop_ratio[6:9])) ## alfred pop
					final_POP.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
					#result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])
				
					Han=float(pop_ratio[6])
					Japan=float(pop_ratio[7])
					Korean=float(pop_ratio[8])

					if ''.join(final_POP) == "EastAsia:Japanese" and abs(Korean-Japan) < 0.1: # and abs(Korean-Han) < 0.2:
						final_POP=["EastAsia:Koreans"]
						result_pop.append("EastAsia:Koreans")

					elif ''.join(final_POP) == "EastAsia:Han":
						final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
						result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

					else:
						final_POP=[Desition_dic[Desition_dic.keys()[6:9][final_index3]]]
						result_pop.append(Desition_dic[Desition_dic.keys()[6:9][final_index3]])

				elif Final_modiPOP == 'EUR': 
					final_index4 = (pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:]).index(max(pop_ratio[9:11]+pop_ratio[13:17]+pop_ratio[20:])) ## alfred pop
					final_POP.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
					result_pop.append(Desition_dic[(Desition_dic.keys()[9:11]+Desition_dic.keys()[13:17]+Desition_dic.keys()[20:])[final_index4]])
				
				elif Final_modiPOP == 'AMR': 
					final_index5 = (pop_ratio[11:13]+pop_ratio[17:20]).index(max(pop_ratio[11:13]+pop_ratio[17:20])) ## alfred pop
					final_POP.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])
					result_pop.append(Desition_dic[(Desition_dic.keys()[11:13]+Desition_dic.keys()[17:20])[final_index5]])

				else:
					final_POP.append("NA")

				Mid_out1_modi_final.write(line.strip()+"\t"+''.join(final_POP)+'\n')

		Mid_out1_modi_final.close()

		return result_pop


	##==========================================================================================================
	## Allele 1
	##==========================================================================================================
	mid_out1=Output_dir+'/'+Sample_id+'_Ancestry_hp1.txt'
	mid_out1_1000=Output_dir+'/'+Sample_id+'_Ancestry_hp1_1000genome.txt'
	mid_out1_modi=Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi.txt'
	mid_out1_modi_final=Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi_final.txt'

	result_pop1=[]
	result_pop1=Algorism_3_calculation_define_population(Chromosome, POP1_dic, POP1_1000, count1_dic, count1_1000, mid_out1, mid_out1_1000, mid_out1_modi, mid_out1_modi_final)

	##==========================================================================================================
	## Allele 2
	##==========================================================================================================
	mid_out2=Output_dir+'/'+Sample_id+'_Ancestry_hp2.txt'
	mid_out2_1000=Output_dir+'/'+Sample_id+'_Ancestry_hp2_1000genome.txt'
	mid_out2_modi=Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi.txt'
	mid_out2_modi_final=Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi_final.txt'

	result_pop2=[]
	result_pop2=Algorism_3_calculation_define_population(Chromosome, POP2_dic, POP2_1000, count2_dic, count2_1000, mid_out2, mid_out2_1000, mid_out2_modi, mid_out2_modi_final)

	result_pop=result_pop1+result_pop2

	##=============================================================================================================
	##
	## Statistics of populations
	##
	##=============================================================================================================
	##print '\n** Ancestry algorithm was done...\n'
	##print '\n** Results are'
	POP_DIVI={'WestEurope': 'EUR', 'NorthAmerica': 'AMR', 'SouthAmerica': 'AMR', 'SouthEurope': 'EUR', 'Africa': 'AFR', 'Asia': 'SAS', 'Oceania': 'EUR', 'NorthEurope': 'EUR', 'EastAsia': 'EAS', 'Siberia': 'EUR', 'EastEurope': 'EUR'}

	out = open(Output_dir+'/'+Sample_id+'_AncestryResult.txt', "w")

	out.write('#Version4.1.2\n')
	#out.write('#'+INPUT_TYPE+'\n')
	#out.write('#Length of using markers (1000g): '+LENGTH_MARKER_1000+"\n")
	#out.write('#Length of using markers (alfred): '+LENGTH_MARKER_alfred+"\n")
	out.write('#Sample_VCF:'+Sample_id+'\n'+'#Report_number:'+Report_num+'\n'+'#User_id:'+User_id+'\n'+'#Geographic_region\tPopulation\tPercentage(%)\n')

	## hp1
	decision_pop={}
	for element in result_pop:
		decision_pop[element]=0

	out_dic={}
	for list_val in decision_pop:
		list_count=[]
		list_count.append(result_pop.count(list_val))
		out_dic[list_val]= (float(list_count[0])/float(len(result_pop)))*100

	out_list=[]
	for key, value in sorted(out_dic.iteritems(), reverse=True, key=lambda (k,v): (v,k)):
		out_list.append("%s: %s" %(key, value))

	cal={}
	for out_li in out_list:
		out_col = out_li.split(":")
		out.write('\t'.join(out_col)+'\n')

		geo, pop, percentage = out_col
		if POP_DIVI[geo] not in cal:
			cal[POP_DIVI[geo]]=[]
		cal[POP_DIVI[geo]].append(float(percentage))

	out.write("#AMR\tEUR\tAFR\tSAS\tEAS\n")
	#out.write(str(round(sum(cal["AMR"]),2))+"\t"+str(round(sum(cal["EUR"]),2))+"\t"+str(round(sum(cal["AFR"]),2))+"\t"+str(round(sum(cal["SAS"]),2))+"\t"+str(round(sum(cal["EAS"]),2))+"\n")

	result_1000=["AMR","EUR","AFR","SAS","EAS"]

	last_val=[]
	for result_1000pop in result_1000:
		if str(result_1000pop.strip()) in cal:
			last_val.append(str(round(sum(cal[str(result_1000pop.strip())]),2)))
		else:
			last_val.append(str('0'))
	out.write("\t".join(last_val))
	out.close()
	##======================================================================================================================
	##
	## Convert output form fo chr map 
	##
	##======================================================================================================================
	#print 'Making **Final POP** output file...'

	final_out = open(Output_dir+'/'+Sample_id+'_FinalPOP.txt','w')

	## read mid_out files
	HP1_result=open(Output_dir+'/'+Sample_id+'_Ancestry_hp1_modi_final.txt', 'r')
	HP2_result=open(Output_dir+'/'+Sample_id+'_Ancestry_hp2_modi_final.txt', 'r')

	#f1=hp1_result.readlines()
	#new_list = "chr1:start:end:europe"
	f1_read = HP1_result.readline()
	new1_list = []
	while(f1_read):
		if not f1_read.startswith("#"):
			new1_line = []
			if len(new1_list) != 0:	
				if new1_list[-1].strip().split(":")[-1] == f1_read.strip().split("\t")[-2].split(":")[0] and new1_list[-1].strip().split(":")[0] == f1_read.strip().split("\t")[0]:
					new1_line.append(new1_list[-1].strip().split(":")[0])
					new1_line.append(new1_list[-1].strip().split(":")[1])
					new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
					new1_line.append(new1_list[-1].strip().split(":")[3])
					new1_list.pop()
				else:
					new1_line.append(f1_read.strip().split("\t")[0])
					new1_line.append(f1_read.strip().split("\t")[1])
					new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
					new1_line.append(f1_read.strip().split("\t")[-2].split(":")[0])
				new1_list.append(":".join(new1_line))
			else:
				new1_line.append(f1_read.strip().split("\t")[0])
				new1_line.append(f1_read.strip().split("\t")[1])
				new1_line.append(str(int(f1_read.strip().split("\t")[1])+win_reading_size))
				new1_line.append(f1_read.strip().split("\t")[-2].split(":")[0])
				new1_list.append(":".join(new1_line))
				
		f1_read = HP1_result.readline()


	f2_read = HP2_result.readline()
	new2_list = []
	while(f2_read):
		if not f2_read.startswith("#"):
			new2_line = []
			if len(new2_list) != 0:	
				if new2_list[-1].strip().split(":")[-1] == f2_read.strip().split("\t")[-2].split(":")[0] and new2_list[-1].strip().split(":")[0] == f2_read.strip().split("\t")[0]:
					new2_line.append(new2_list[-1].strip().split(":")[0])
					new2_line.append(new2_list[-1].strip().split(":")[1])
					new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
					new2_line.append(new2_list[-1].strip().split(":")[3])
					new2_list.pop()
				else:
					new2_line.append(f2_read.strip().split("\t")[0])
					new2_line.append(f2_read.strip().split("\t")[1])
					new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
					new2_line.append(f2_read.split("\t")[-2].split(":")[0])
				new2_list.append(":".join(new2_line))
			else:
				new2_line.append(f2_read.strip().split("\t")[0])
				new2_line.append(f2_read.strip().split("\t")[1])
				new2_line.append(str(int(f2_read.strip().split("\t")[1])+win_reading_size))
				new2_line.append(f2_read.strip().split("\t")[-2].split(":")[0])
				new2_list.append(":".join(new2_line))
				
		f2_read = HP2_result.readline()

	for result1 in new1_list:
		ls=result1.split(":")
		Chr, Bin_start, Bin_end, Pop = ls[0:]
		final_out.write(Chr +'_1'+ '\t'+ Bin_start +'-'+ Bin_end +'\t'+ Pop.strip().upper() +'\n')

	for result2 in new2_list:
		ls=result2.split(":")
		Chr, Bin_start, Bin_end, Pop = ls[0:]
		final_out.write(Chr +'_2'+ '\t'+ Bin_start +'-'+ Bin_end +'\t'+ Pop.strip().upper() +'\n')

	final_out.close()

	os.system("sort -k1V "+ Output_dir+"/"+Sample_id+"_FinalPOP.txt > "+Output_dir+"/sorted_"+Sample_id+"_FinalPOP.txt")


	os.system("rm -f "+Output_dir+"/"+Sample_id+"_FinalPOP.txt")
	os.system("rm -f "+Temp_dir+"/"+Input_f.split("/")[-1].split(".vcf")[0]+"_mat.vcf")
	os.system("rm -f "+Temp_dir+"/"+eagle_out)
	os.system("rm -f "+Temp_dir+"/"+Sample_id+".eagle.log")
	os.system("rm -f "+Temp_dir+"/"+Sample_id+".final.vcf")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1_modi.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2_modi.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp1_modi_final.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+"_Ancestry_hp2_modi_final.txt")
	os.system("rm -f "+ Output_dir+'/'+Sample_id+'_Ancestry_hp1_1000genome.txt')
	os.system("rm -f "+ Output_dir+'/'+Sample_id+'_Ancestry_hp2_1000genome.txt')

	os.system("rm -f "+ Temp_dir+"/"+Sample_id+"_mat_af_1000genome_database.txt")

	os.system("rm -f "+ Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_ref.txt")
	os.system("rm -f "+ Temp_dir+"/"+Sample_id+"_mat_POP24_alfred_parsing_alt.txt")


	os.system("rm -f "+Temp_dir+"/eagle."+Sample_id+".vcf.gz")
	os.system("rm -f "+Temp_dir+"/eagle."+Sample_id+".vcf.gz.tbi")

	os.system("rm -f "+Temp_dir+"/"+eagle_out+".vcf")

	os.system("cat "+Output_dir+"/"+Sample_id+"_AncestryResult.txt > "+Output_dir+"/"+Report_num+".txt")
	os.system("echo '--------------------------------------------------' >> "+Output_dir+"/"+Report_num+".txt")
	os.system("cat "+Output_dir+"/sorted_"+Sample_id+'_FinalPOP.txt'+" >> "+Output_dir+"/"+Report_num+".txt")

	print "alrp^" + Report_num + "^" + Output_dir+'/'+Report_num+".txt"+"^alend;"
	print "aled^"+ Report_num +"^end^alend;"


except Exception as er:
	if Report_num is not None:
		print 'aler^' + Report_num + '^' + Err_m[1]+" "+`er` + '^alend;'
	else:
		print 'aler^' + 'None' + '^' + Err_m[1]+" "+`er` + '^alend;'

#os.system("cat ./html_head.txt "+Output_dir+"/sorted_"+Sample_id+"_FinalPOP.txt  ./html_tail.txt > "+Output_dir+"/html_graph_"+Sample_id+".html")
